  cmake_minimum_required(VERSION 3.10)
  set(CMAKE_VERBOSE_MAKEFILE ON)

  project(Blasteroids)

  if (MSVC)
    # warning level 4 and all warnings as errors
    add_compile_options(/W4 /WX)
  else()
    # lots of warnings and all warnings as errors
    add_compile_options(-Wall -Wextra -Wpedantic -Werror)
  endif()

  find_package(PkgConfig REQUIRED)
  pkg_check_modules(allegro-5 REQUIRED
    allegro-5
    allegro_font-5
    allegro_primitives-5
    allegro_audio-5
    allegro_acodec-5
  )

  add_library(entities
    asteroid.c
    spaceship.c
    blast.c
  )
  target_include_directories(entities PUBLIC
    "${PROJECT_SOURCE_DIR}"
  )
  target_link_libraries(entities
    ${allegro-5_LIBRARIES}
  )
  # target_compile_definitions(entities PUBLIC DEBUG)

  add_library(utils
    utils.c
  )
  target_include_directories(utils PUBLIC
    "${PROJECT_SOURCE_DIR}"
  )

  add_executable(blasteroids
    blasteroids.c
  )
  target_include_directories(blasteroids PUBLIC
    ${allegro-5_INCLUDE_DIRS}
    "${PROJECT_SOURCE_DIR}"
  )
  target_link_directories(blasteroids PUBLIC
    "${PROJECT_SOURCE_DIR}"
  )
  target_link_libraries(blasteroids
    ${allegro-5_LIBRARIES}
    entities
    m # math
    utils
  )
  # target_compile_definitions(blasteroids PUBLIC DEBUG)
