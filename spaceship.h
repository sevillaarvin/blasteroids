  #ifndef spaceship_h
  #define spaceship_h

  #include <allegro5/allegro5.h>
  #include <common.h>

  typedef struct {
    float sx;
    float sy;
    float heading;
    float speed;
    int gone;
    ALLEGRO_COLOR color;
  } Spaceship;

  Spaceship *ship_create(float x, float y);
  void ship_destroy(Spaceship *s);

  void ship_paint(Spaceship *s);
  void ship_move(Spaceship *s);
  void ship_reset(Spaceship *s, float x, float y);

  void ship_speed_up(Spaceship *s);
  void ship_slow_down(Spaceship *s);
  void ship_turn_left(Spaceship *s);
  void ship_turn_right(Spaceship *s);

  struct bounds ship_bounds(Spaceship *s);

  #endif
