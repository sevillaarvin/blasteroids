  #include <math.h>
  #include <allegro5/allegro_primitives.h>
  #include <graphviz/arith.h>

  #include <blast.h>
  #include <constants.h>
  #include <debug.h>

  Blast *blast_create()
  {
    Blast *b = (Blast *) malloc(sizeof(Blast));
    b->sx = 0.0f;
    b->sy = 0.0f;
    b->speed = 0.0f;
    b->gone = 1;
    b->color = al_map_rgb(0, 0, 255); // TODO
    return b;
  }

  void blast_destroy(Blast *b)
  {
    free(b);
  }

  void blast_shoot(Blast *b, float fromx, float fromy, float heading)
  {
    b->sx = fromx;
    b->sy = fromy;
    b->heading = heading;
    b->gone = 0;
    b->speed = BLAST_SPEED_MAX;
  }

  void blast_impact(Blast *b)
  {
    b->gone = 1;
  }

  void blast_paint(Blast *b)
  {
    ALLEGRO_TRANSFORM transform;
    al_identity_transform(&transform);
    al_rotate_transform(&transform, RADIANS(b->heading) + M_PI_2);
    al_translate_transform(&transform, b->sx, b->sy);
    al_use_transform(&transform);

    al_draw_line(0, -(BLAST_SIZE / 2), 0, (BLAST_SIZE / 2), b->color, 3.0f);

    D(
      struct bounds aa = blast_bounds(b);
      ALLEGRO_COLOR ac = al_map_rgb(0, 255, 0);

      al_identity_transform(&transform);
      al_use_transform(&transform);
      al_draw_line(aa.x           ,             aa.y, aa.x + aa.width,             aa.y, ac, 2.0f);
      al_draw_line(aa.x + aa.width,             aa.y, aa.x + aa.width, aa.y + aa.height, ac, 2.0f);
      al_draw_line(aa.x + aa.width, aa.y + aa.height,            aa.x, aa.y + aa.height, ac, 2.0f);
      al_draw_line(           aa.x, aa.y + aa.height,            aa.x,             aa.y, ac, 2.0f);
      );
  }

  void blast_move(Blast *b)
  {
    float x = cosf(RADIANS(b->heading)) * b->speed;
    b->sx += x;

    float y = sinf(RADIANS(b->heading)) * b->speed;
    b->sy += y;
  }

  struct bounds blast_bounds(Blast *b)
  {
    float x = sinf(RADIANS(b->heading) + M_PI_2);
    float y = cosf(RADIANS(b->heading) + M_PI_2);

    return (struct bounds) {
      .x = b->sx - ((BLAST_SIZE / 2) * (x >= 0 ? x : -x)),
      .y = b->sy - ((BLAST_SIZE / 2) * (y >= 0 ? y : -y)),
      .width = BLAST_SIZE * ((x >= 0 ? x : -x)),
      .height = BLAST_SIZE * ((y >= 0 ? y : -y))
    };
  }
