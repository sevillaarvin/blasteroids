  #ifndef asteroid_h
  #define asteroid_h

  #include <allegro5/allegro5.h>
  #include <common.h>

  typedef struct {
    float sx;
    float sy;
    float heading;
    float twist;
    float speed;
    float rot_velocity;
    float scale;
    int gone;
    ALLEGRO_COLOR color;
  } Asteroid;

  Asteroid *asteroid_create(float x,
                            float y,
                            float speed,
                            float heading,
                            float rot_velocity);

  void asteroid_destroy(Asteroid *a);

  void asteroid_move(Asteroid *a);
  void asteroid_paint(Asteroid *a);
  struct bounds asteroid_bounds(Asteroid *a);

  void asteroid_split(Asteroid *a, Asteroid *a1, Asteroid *a2);
  void asteroid_kill(Asteroid *a);
  void asteroid_revive(Asteroid *a);

  #endif
