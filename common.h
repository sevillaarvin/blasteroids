  #ifndef common_h
  #define common_h

  struct bounds {
    float x;
    float y;
    float width;
    float height;
  };

  #endif
