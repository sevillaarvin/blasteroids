  #include <allegro5/allegro_primitives.h>
  #include <graphviz/arith.h>

  #include <asteroid.h>
  #include <constants.h>
  #include <utils.h>
  #include <debug.h>

  Asteroid *asteroid_create(float x,
                            float y,
                            float speed,
                            float heading,
                            float rot_velocity)
  {
    Asteroid *a = (Asteroid *) malloc(sizeof(Asteroid));
    a->sx = x;
    a->sy = y;
    a->speed = speed;
    a->color = al_map_rgb(0xff, 0xff, 0xff);
    a->heading = heading;
    a->twist = 0.0f;
    a->rot_velocity = rot_velocity;
    a->scale = 1.0f;
    a->gone = 0.0f;
    return a;
  }

  void asteroid_destroy(Asteroid *a)
  {
    free(a);
  }

  void asteroid_move(Asteroid *a)
  {
    float x = cosf(RADIANS(a->heading)) * a->speed;
    a->sx += x;
    if (a->sx < 0) a->sx += GAME_WIDTH;
    if (a->sx > GAME_WIDTH) a->sx -= GAME_WIDTH;

    float y = sinf(RADIANS(a->heading)) * a->speed;
    a->sy += y;
    if (a->sy < 0) a->sy += GAME_HEIGHT;
    if (a->sy > GAME_HEIGHT) a->sy -= GAME_HEIGHT;

    a->twist += a->rot_velocity;
  }

  void asteroid_paint(Asteroid *a)
  {
    ALLEGRO_TRANSFORM transform;
    al_identity_transform(&transform);
    al_rotate_transform(&transform, RADIANS(a->twist) + M_PI_2);
    al_scale_transform(&transform, a->scale, a->scale);
    al_translate_transform(&transform, a->sx, a->sy);
    al_use_transform(&transform);

    al_draw_line(-20,  20, -25,   5, a->color, 2.0f);
    al_draw_line(-25,   5, -25, -10, a->color, 2.0f);
    al_draw_line(-25, -10,  -5, -10, a->color, 2.0f);
    al_draw_line( -5, -10, -10, -20, a->color, 2.0f);
    al_draw_line(-10, -20,   5, -20, a->color, 2.0f);
    al_draw_line(  5, -20,  20, -10, a->color, 2.0f);
    al_draw_line( 20, -10,  20,  -5, a->color, 2.0f);
    al_draw_line( 20,  -5,   0,   0, a->color, 2.0f);
    al_draw_line(  0,   0,  20,  10, a->color, 2.0f);
    al_draw_line( 20,  10,  10,  20, a->color, 2.0f);
    al_draw_line( 10,  20,   0,  15, a->color, 2.0f);
    al_draw_line(  0,  15, -20,  20, a->color, 2.0f);

    D(
      struct bounds aa = asteroid_bounds(a);
      ALLEGRO_COLOR ac = al_map_rgb(0, 255, 0);

      al_identity_transform(&transform);
      al_use_transform(&transform);
      al_draw_line(aa.x           ,             aa.y, aa.x + aa.width,             aa.y, ac, 2.0f);
      al_draw_line(aa.x + aa.width,             aa.y, aa.x + aa.width, aa.y + aa.height, ac, 2.0f);
      al_draw_line(aa.x + aa.width, aa.y + aa.height,            aa.x, aa.y + aa.height, ac, 2.0f);
      al_draw_line(           aa.x, aa.y + aa.height,            aa.x,             aa.y, ac, 2.0f);
      );
  }

  struct bounds asteroid_bounds(Asteroid *a)
  {
    return (struct bounds) {
      .x = a->sx - (25.0f * a->scale),
      .y = a->sy - (25.0f * a->scale),
      .width = 45.0f * a->scale,
      .height = 45.0f * a->scale
    };
  }

  void asteroid_split(Asteroid *a, Asteroid *a1, Asteroid *a2)
  {
    a1->sx = a->sx;
    a1->sy = a->sy + num_between(-20.0f, 20.0f);

    a2->sx = a->sx;
    a2->sy = a->sy - num_between(-20.0f, 20.0f);

    /* a1->scale = a2->scale = a->scale / 2.0f; */
    a1->scale = a2->scale = a->scale * 0.70710f;

    a1->gone = a2->gone = !(a->gone = 1);
  }

  void asteroid_kill(Asteroid *a)
  {
    a->gone = 1;
  }

  void asteroid_revive(Asteroid *a)
  {
    a->gone = 0;
  }
