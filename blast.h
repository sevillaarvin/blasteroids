  #ifndef blast_h
  #define blast_h

  #define BLAST_SIZE 10.0f

  #include <allegro5/allegro5.h>
  #include <common.h>

  typedef struct {
    float sx;
    float sy;
    float heading;
    float speed;
    int gone;
    ALLEGRO_COLOR color;
  } Blast;

  Blast *blast_create();
  void blast_destroy(Blast *b);
  void blast_shoot(Blast *b, float fromx, float fromy, float heading);
  void blast_impact(Blast *b);

  void blast_move(Blast *b);
  void blast_paint(Blast *b);

  struct bounds blast_bounds(Blast *b);

  #endif
