  #include <stdio.h>
  #include <errno.h>
  #include <string.h>
  #include <time.h>

  #include <graphviz/arith.h>
  #include <allegro5/allegro5.h>
  #include <allegro5/allegro_font.h>
  #include <allegro5/allegro_audio.h>
  #include <allegro5/allegro_acodec.h>

  #include <asteroid.h>
  #include <blast.h>
  #include <constants.h>
  #include <utils.h>
  #include <debug.h>

  int main(int argc, char **argv)
  {
    int seed;
    if(argc > 1)
      seed = atoi(argv[1]);
    else
      seed = (int) time(NULL);
    srandom(seed);

    ensure(al_init(), "Could not initialize game");
    ensure(al_install_keyboard(), "Could not detect keyboard");

    ALLEGRO_TIMER *timer = al_create_timer(1.0 / GAME_FPS);
    ensure(timer, "Could not create timer");

    ALLEGRO_EVENT_QUEUE *queue = al_create_event_queue();
    ensure(queue, "Could not create event queue");

    ALLEGRO_DISPLAY *disp = al_create_display(GAME_WIDTH, GAME_HEIGHT);
    ensure(disp, "Could not create display");

    ALLEGRO_FONT *font = al_create_builtin_font();
    ensure(font, "Could not create font");

    ensure(al_install_audio(), "audio");
    ensure(al_init_acodec_addon(), "audio codecs");
    ensure(al_reserve_samples(16), "reserve samples");

    ALLEGRO_SAMPLE* engine = al_load_sample(SOUND_ENGINE);
    ensure(engine, "couldn't load engine");
    ALLEGRO_SAMPLE_INSTANCE* engine_instance = al_create_sample_instance(engine);
    ensure(engine_instance, "couldn't load engine instance");
    ensure(al_attach_sample_instance_to_mixer(engine_instance, al_get_default_mixer()),
           "couldn't attach engine instance to mixer");
    ensure(al_set_sample_instance_playmode(engine_instance, ALLEGRO_PLAYMODE_LOOP),
           "couldn't set engine playmode");

    ALLEGRO_SAMPLE* laser = al_load_sample(SOUND_BLAST);
    ensure(laser, "couldn't load laser");

    ALLEGRO_SAMPLE* explode = al_load_sample(SOUND_EXPLODE);
    ensure(explode, "couldn't load explode");

    ALLEGRO_SAMPLE* impact = al_load_sample(SOUND_IMPACT);
    ensure(impact, "couldn't load impact");

    // TODO handle errors here
    al_register_event_source(queue, al_get_keyboard_event_source());
    al_register_event_source(queue, al_get_display_event_source(disp));
    al_register_event_source(queue, al_get_timer_event_source(timer));

    bool done = false;
    bool redraw = true;
    ALLEGRO_EVENT event;
    unsigned char key[ALLEGRO_KEY_MAX];
    memset(key, 0, sizeof(key));

    Player *player = player_create(ship_create(GAME_WIDTH / 2, GAME_HEIGHT / 2));

    Asteroid *asteroids[ASTEROID_SPLIT_MAX][ASTEROID_WHOLE_MAX];
    for(int i = 0; i < ASTEROID_SPLIT_MAX; ++i)
      for(int j = 0; j < ASTEROID_WHOLE_MAX; ++j) {
        float x = num_between(0.0f, (float) GAME_WIDTH);
        float y = num_between(0.0f, (float) GAME_HEIGHT);
        float speed = num_between(1.0f, (float) ASTEROID_SPEED_MAX);
        float heading = num_between(0.0f, 360.0f);
        float rot_velocity = num_between(-1.0f, 1.0f);
        D(fprintf(stderr, "x: %f, y: %f, s: %f, h: %f, r: %f\n", x, y, speed, heading, rot_velocity););
        asteroids[i][j] = asteroid_create(x, y, speed, heading, rot_velocity);

        if (i == 0) continue;

        asteroid_kill(asteroids[i][j]);
      }

    Blast *blasts[BLAST_MAX];
    for(int i = 0; i < BLAST_MAX; ++i)
      blasts[i] = blast_create();
    int cblast = 0;
    double blast_last_at = 0;

    al_start_timer(timer);
    while(1)
    {
      al_wait_for_event(queue, &event);

      switch(event.type) {
      case ALLEGRO_EVENT_TIMER:
        if(key[ALLEGRO_KEY_W]) {
          ship_speed_up(player->spaceship);
          if(!al_get_sample_instance_playing(engine_instance))
            al_set_sample_instance_playing(engine_instance, 1);
          else
            al_set_sample_instance_speed(engine_instance, player->spaceship->speed / SHIP_SPEED_MAX * 3);
        }
        if(key[ALLEGRO_KEY_A])
          ship_turn_left(player->spaceship);
        if(key[ALLEGRO_KEY_S]) {
          ship_slow_down(player->spaceship);
          if(player->spaceship->speed <= 0 && al_get_sample_instance_playing(engine_instance))
            al_set_sample_instance_playing(engine_instance, 0);
          else
            al_set_sample_instance_speed(engine_instance, player->spaceship->speed / SHIP_SPEED_MAX * 3);
        }
        if(key[ALLEGRO_KEY_D])
          ship_turn_right(player->spaceship);
        if(key[ALLEGRO_KEY_R])
          game_restart(player, asteroids);
        if(key[ALLEGRO_KEY_ESCAPE])
          done = true;

        for(int i = 0; i < ALLEGRO_KEY_MAX; ++i)
          key[i] &= KEY_SEEN;

        ship_move(player->spaceship);

        for(size_t i = 0; i < BLAST_MAX; ++i) {
          if(blasts[i]->gone) continue;

          blast_move(blasts[i]);
          if(is_out_of_screen(blasts[i]))
            blasts[i]->gone = 1;
        }

        for(size_t i = 0; i < ASTEROID_SPLIT_MAX; ++i)
          for(size_t j = 0; j < ASTEROID_WHOLE_MAX; ++j) {
            if(asteroids[i][j]->gone) continue;
            asteroid_move(asteroids[i][j]);

            double time_until_vuln = difftime(time(NULL), player->spawned_at + (time_t) LIVES_INVULN_SEC);

            D(
              if((int) event.any.timestamp % 2)
                fprintf(stderr,
                        "now: %li, vuln: %li, res: %f\n",
                        time(NULL),
                        player->spawned_at + (time_t) LIVES_INVULN_SEC,
                        time_until_vuln);
              else
                NULL;
              );

            if(time_until_vuln > 0 &&
               player->lives &&
               has_collided(player->spaceship, asteroids[i][j])) {
              al_play_sample(explode, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

              player->lives--;

              Spaceship *s = player->spaceship;
              if(player->lives != 0) {
                ship_reset(player->spaceship, GAME_WIDTH / 2, GAME_HEIGHT / 2);
                player->spawned_at = time(NULL);
              }
              else
                s->gone = 1;
            }

            for(size_t k = 0; k < BLAST_MAX; ++k) {
              if(blasts[k]->gone) continue;

              if(!has_collided(blasts[k], asteroids[i][j])) continue;

              al_play_sample(impact, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

              blasts[k]->gone = 1;
              player->score += 100;

              if(i == 0)
                asteroid_split(asteroids[i][j], asteroids[i + 1][j], asteroids[i + 2][j]);
              else
                asteroid_kill(asteroids[i][j]);

              break;
            }
          }

        redraw = true;
        break;

      case ALLEGRO_EVENT_KEY_CHAR:
        switch(event.keyboard.keycode) {
        case ALLEGRO_KEY_SPACE:
          if(!player->lives) break;
          if((event.any.timestamp - blast_last_at) < 0.2) break;

          D(fprintf(stderr, "blast: %f\n", event.any.timestamp););

          Spaceship *s = player->spaceship;
          blast_shoot(blasts[cblast], s->sx, s->sy, s->heading);
          blast_last_at = event.any.timestamp;
          cblast = (cblast + 1) % BLAST_MAX;

          al_play_sample(laser, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
          break;
        }
        break;

      case ALLEGRO_EVENT_KEY_DOWN:
        key[event.keyboard.keycode] = KEY_SEEN | KEY_RELEASE;
        break;
      case ALLEGRO_EVENT_KEY_UP:
        key[event.keyboard.keycode] &= KEY_RELEASE;
        break;
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
        done = true;
        break;
      }

      if(done) break;

      if(redraw && al_is_event_queue_empty(queue)) {
        al_clear_to_color(al_map_rgb(0, 0, 0));

        show_panel(font, player);

        for(size_t i = 0; i < BLAST_MAX; ++i) {
          if(!blasts[i]->gone) {
            blast_paint(blasts[i]);
          }
        }

        for(size_t i = 0; i < ASTEROID_SPLIT_MAX; ++i)
          for(size_t j = 0; j < ASTEROID_WHOLE_MAX; ++j)
            if(!asteroids[i][j]->gone)
              asteroid_paint(asteroids[i][j]);

        if(player->lives)
          ship_paint(player->spaceship);
        else {
          show_game_over(font);
          al_set_sample_instance_playing(engine_instance, 0);
        }

        al_flip_display();

        redraw = false;
      }
    }

    for(int i = 0; i < BLAST_MAX; ++i)
      blast_destroy(blasts[i]);

    for(int i = 0; i < ASTEROID_SPLIT_MAX; ++i)
      for(int j = 0; j < ASTEROID_WHOLE_MAX; ++j)
        asteroid_destroy(asteroids[i][j]);

    player_destroy(player);

    al_destroy_sample(impact);
    al_destroy_sample(explode);
    al_destroy_sample(laser);
    al_destroy_sample_instance(engine_instance);
    al_destroy_sample(engine);

    al_destroy_font(font);
    al_destroy_display(disp);
    al_destroy_timer(timer);
    al_destroy_event_queue(queue);

    puts("Bye!");
    return 0;
  }
