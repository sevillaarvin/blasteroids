  #include <allegro5/allegro_font.h>

  #include <asteroid.h>
  #include <blast.h>
  #include <spaceship.h>

  int _has_collided_s_a(Spaceship *s, Asteroid *a);
  int _has_collided_b_a(Blast *b, Asteroid *a);

  #define has_collided(a, b) _Generic((a), \
                                      Spaceship*:_has_collided_s_a, \
                                      Blast*:_has_collided_b_a)(a, b)

  #define ensure(fn, msg) do { if(!fn) { error(msg); } } while(0)

  typedef struct {
    Spaceship *spaceship;
    int score;
    int lives;
    time_t spawned_at;
  } Player;

  void error(char * msg);

  void show_panel(ALLEGRO_FONT *font, Player *player);
  void show_game_over(ALLEGRO_FONT *font);
  void game_restart(Player *p, Asteroid *as[ASTEROID_SPLIT_MAX][ASTEROID_WHOLE_MAX]);
  int is_out_of_screen(Blast *b);

  Player *player_create(Spaceship *s);
  void player_destroy(Player *p);

  float num_between(float beg, float end);
