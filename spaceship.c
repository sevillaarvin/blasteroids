  #include <math.h>
  #include <allegro5/allegro_primitives.h>
  #include <graphviz/arith.h>

  #include <spaceship.h>
  #include <constants.h>
  #include <debug.h>

  Spaceship *ship_create(float x, float y)
  {
    Spaceship *s = (Spaceship *) malloc(sizeof(Spaceship));
    s->sx = x;
    s->sy = y;
    s->heading = -DEGREES(M_PI_2);
    s->gone = 0;
    s->color = al_map_rgb(0, 255, 0);
    return s;
  }

  void ship_destroy(Spaceship *s)
  {
    free(s);
  }

  void ship_paint(Spaceship *s)
  {
    ALLEGRO_TRANSFORM transform;
    al_identity_transform(&transform);
    al_rotate_transform(&transform, RADIANS(s->heading) + M_PI_2);
    al_translate_transform(&transform, s->sx, s->sy);
    al_use_transform(&transform);

    al_draw_line(-8,   9,  0, -11, s->color, 3.0f);
    al_draw_line( 0, -11,  8,   9, s->color, 3.0f);
    al_draw_line(-6,   4, -1,   4, s->color, 3.0f);
    al_draw_line( 6,   4,  1,   4, s->color, 3.0f);

    D(
      struct bounds aa = ship_bounds(s);
      ALLEGRO_COLOR ac = al_map_rgb(255, 0, 0);

      al_identity_transform(&transform);
      al_use_transform(&transform);
      al_draw_line(aa.x           ,             aa.y, aa.x + aa.width,             aa.y, ac, 2.0f);
      al_draw_line(aa.x + aa.width,             aa.y, aa.x + aa.width, aa.y + aa.height, ac, 2.0f);
      al_draw_line(aa.x + aa.width, aa.y + aa.height,            aa.x, aa.y + aa.height, ac, 2.0f);
      al_draw_line(           aa.x, aa.y + aa.height,            aa.x,             aa.y, ac, 2.0f);
      );
  }

  void ship_move(Spaceship *s)
  {
    float x = cosf(RADIANS(s->heading)) * s->speed;
    s->sx += x;
    if (s->sx < 0) s->sx += GAME_WIDTH;
    if (s->sx > GAME_WIDTH) s->sx -= GAME_WIDTH;

    float y = sinf(RADIANS(s->heading)) * s->speed;
    s->sy += y;
    if (s->sy < 0) s->sy += GAME_HEIGHT;
    if (s->sy > GAME_HEIGHT) s->sy -= GAME_HEIGHT;
  }

  void ship_reset(Spaceship *s, float x, float y)
  {
    s->sx = x;
    s->sy = y;
    s->gone = 0;
    s->speed = 0.0f;
    s->heading = -DEGREES(M_PI_2);
  }

  void ship_speed_up(Spaceship *s)
  {
    if (s->speed < SHIP_SPEED_MAX) {
      s->speed++;
    }
  }

  void ship_slow_down(Spaceship *s)
  {
    if (s->speed > 0) {
      s->speed--;
    }
  }

  void ship_turn_left(Spaceship *s)
  {
    s->heading -= 10.0f;
    if (s->heading < 0.0f) {
      s->heading = 360 + s->heading;
    }
  }

  void ship_turn_right(Spaceship *s)
  {
    s->heading += 10.0f;
    if (s->heading > 360.0f) {
      s->heading = s->heading - 360.0f;
    }
  }

  struct bounds ship_bounds(Spaceship *s)
  {
    return (struct bounds) {
      .x = s->sx - 8.0f,
      .y = s->sy - 11.0f,
      .width = 20.0f,
      .height = 20.0f
    };
  }
