  #include <stdio.h>
  #include <time.h>
  #include <graphviz/arith.h>

  #include <constants.h>
  #include <utils.h>
  #include <common.h>
  #include <debug.h>

  static void show_score(unsigned int score, ALLEGRO_FONT *font);
  static void show_lives(unsigned short lives);

  void error(char * msg)
  {
    fprintf(stderr, "%s: %s\n", msg, strerror(errno));
    exit(1);
  }

  int _has_collided_s_a(Spaceship *s, Asteroid *a)
  {
    struct bounds sb = ship_bounds(s);
    struct bounds ab = asteroid_bounds(a);

    int result = !!(// the left of the ship is in between the asteroid
                    ((ab.x < sb.x && sb.x < (ab.x + ab.width)) ||
                     // the right of the ship is in between the asteroid
                     (ab.x < (sb.x + sb.width) && (sb.x + sb.width) < (ab.x + ab.width))) &&
                    // the top of the ship is in between the asteroid
                    ((ab.y < sb.y && sb.y < (ab.y + ab.height)) ||
                     // the bottom of the ship is in between the asteroid
                     (ab.y < (sb.y + sb.height) && (sb.y + sb.height) < (ab.y + ab.height))));

    return result;
  }

  int _has_collided_b_a(Blast *b, Asteroid *a)
  {
    struct bounds bb = blast_bounds(b);
    struct bounds ab = asteroid_bounds(a);

    int result = !!(// the left of the ship is in between the asteroid
                    ((ab.x < bb.x && bb.x < (ab.x + ab.width)) ||
                     // the right of the ship is in between the asteroid
                     (ab.x < (bb.x + bb.width) && (bb.x + bb.width) < (ab.x + ab.width))) &&
                    // the top of the ship is in between the asteroid
                    ((ab.y < bb.y && bb.y < (ab.y + ab.height)) ||
                     // the bottom of the ship is in between the asteroid
                     (ab.y < (bb.y + bb.height) && (bb.y + bb.height) < (ab.y + ab.height))));

    return result;
  }

  void show_panel(ALLEGRO_FONT *font, Player *player)
  {
    show_score(player->score, font);
    show_lives(player->lives);
  }

  static void show_score(unsigned int score, ALLEGRO_FONT *font)
  {
    ALLEGRO_TRANSFORM transform;
    al_identity_transform(&transform);
    al_scale_transform(&transform, 2.0, 2.0);
    al_use_transform(&transform);

    char s[16];
    sprintf(s, "%i", score);
    al_draw_text(font, al_map_rgb(255, 255, 255), 8, 8, 0, s);
  }

  static void show_lives(unsigned short lives)
  {
    Spaceship *s[LIVES_MAX];
    for (int i = 0; i < lives; ++i) {
      s[i] = ship_create(32 + (i * 32), 64);
    }

    for (int i = 0; i < lives; ++i) {
      ship_paint(s[i]);
    }

    for (int i = 0; i < lives; ++i) {
      ship_destroy(s[i]);
    }
  }

  void show_game_over(ALLEGRO_FONT *font)
  {
    ALLEGRO_TRANSFORM transform;
    al_identity_transform(&transform);
    al_scale_transform(&transform, 8, 8);
    al_translate_transform(&transform, GAME_WIDTH / 2, GAME_HEIGHT / 2);
    al_use_transform(&transform);

    al_draw_text(font, al_map_rgb(255, 0, 0), -36, -4, 0, "GAME OVER");
  }

  void game_restart(Player *p, Asteroid *as[ASTEROID_SPLIT_MAX][ASTEROID_WHOLE_MAX])
  {
    p->lives = LIVES_MAX;
    p->score = 0;
    p->spawned_at = time(NULL);
    ship_reset(p->spaceship, (float) (GAME_WIDTH / 2), (float) (GAME_HEIGHT / 2));

    for(int i = 0; i < ASTEROID_SPLIT_MAX; ++i)
      for(int j = 0; j < ASTEROID_WHOLE_MAX; ++j)
        if(i == 0)
          asteroid_revive(as[i][j]);
        else
          asteroid_kill(as[i][j]);
  }

  int is_out_of_screen(Blast *b)
  {
    return (b->sx > (float) GAME_WIDTH ||
            b->sx < 0.0f ||
            b->sy > (float) GAME_HEIGHT ||
            b->sy < 0.0f);
  }

  Player *player_create(Spaceship *s)
  {
    Player *p = (Player *) malloc(sizeof(Player));
    p->spaceship = s;
    p->score = 0;
    p->lives = LIVES_MAX;
    p->spawned_at = time(NULL);

    return p;
  }

  void player_destroy(Player *p)
  {
    free(p);
  }

  float num_between(float beg, float end)
  {
    long r = random();
    float ratio = (float) r / (float) INT_MAX;
    float result = ((end - beg) * ratio) + beg;

    return result;
  }
