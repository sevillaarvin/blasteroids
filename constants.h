  #ifndef constants_h
  #define constants_h

  #define GAME_WIDTH 800
  #define GAME_HEIGHT 600

  #define GAME_FPS 30.0

  #define KEY_SEEN 1
  #define KEY_RELEASE 2

  #define LIVES_MAX 3
  #define LIVES_INVULN_SEC 5
  #define BLAST_MAX (GAME_HEIGHT / 2)

  #define ASTEROID_SPLIT_MAX 3
  #define ASTEROID_WHOLE_MAX 10
  #define ASTEROID_SPEED_MAX 4.0f
  #define SHIP_SPEED_MAX (ASTEROID_SPEED_MAX * 1.5f)
  #define BLAST_SPEED_MAX (ASTEROID_SPEED_MAX * 3.0f)

  #define SOUND_ENGINE "./assets/spaceship.wav"
  #define SOUND_BLAST "./assets/blast.wav"
  #define SOUND_EXPLODE "./assets/explode.wav"
  #define SOUND_IMPACT "./assets/impact.wav"

  #endif
